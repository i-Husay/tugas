<?php
require_once("animal.php");
require_once('frog.php');
require_once("ape.php");
$sheep = new animal("shaun");

echo "Animal Name : $sheep->name <br>"; // "shaun"
echo "Legs  : $sheep->legs <br>"; // 4
echo "Cold Blooded : $sheep->cold_blooded <br><br>"; // "no"

$kodok = new frog("buduk");
echo "Animal Name : $kodok->name <br>";
echo "Legs  : $kodok->legs<br>";
echo "Cold Blooded : $kodok->cold_blooded <br>";
echo "Jump  : ".$kodok->jump()."<br><br>";

$sungokong = new ape("sungokong");
echo "Animal Name : $sungokong->name <br>";
echo "Legs  : $sungokong->legs<br>";
echo "Cold Blooded : $sungokong->cold_blooded <br>";
echo "Yell  : ".$sungokong->yell()."<br><br>";
?>